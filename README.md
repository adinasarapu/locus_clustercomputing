In this document we will go through all of the steps of RNA-Seq differential expression analysis using NIH/NIAID High Performance Computing (HPC), Locus Computing Cluster, which will include:

1. Connect HPC Server via SSH

SSH allows you to connect to Locus Cluster server securely and perform linux command-line operations.
`ssh dinasarapuar@ai-submit1.niaid.nih.gov`

2. Quality control of raw reads, FastQC

Merging of sample specific raw (FASTQ) data into a single FASTQ file was performed. Each of Batch1 (N=52) sample ran on 9 lanes while Batch2 (N=93) sample ran on 16 lanes to get 50 Million reads. The raw reads from both datasets (batch1 and batch2) were quality assessed (no major quality issues were observed).

Create a shell script called `merge_and_fastqc.sh` in your `script` sub-directory and submit this job from `logs` sub-directory.

3. Generation of Alignments and Alignment QC

Reads obtained from RNA sequencing were mapped against the reference genome of Homo sapiens (UCSC hg38 build) utilizing information from the gene transfer format (.gtf) annotation from hg38. The overall RNA-Seq read mapping (hg38 reference) rate is pretty good with some exceptions.  9 samples showed significant multimapping of reads (report attached). If the data is "total" rRNA depleted RNA, then the most likely reason for increased multimappers is incomplete rRNA depletion. 2 samples (HC0052 and 100199) have 38 and 37 Million reads instead 50 Million. If we set minimum uniquely mapped reads as our cutoff (>= 85%), we need to exclude 14 samples from further analysis (for time being!!!).  


3. Generation of counts - Counts (or reads) that overlap known features (here genes) are obtained (146 samples, ~26, 000 genes).

Next step is to select samples (e.g, case and control) for differential expression analysis and perform the following steps.

4. Filtering of genes, normalization, counts per million (CPM) and log2-transformation of CPM values.
5. Cluster/Correlation analysis, PCA and/or MDS plots (to see batch effects) will be performed.

6. Analysis of differential gene expression – With normalized data, tests for differential expression between two groups will be performed using a method similar in idea to the Fisher's Exact Test. This test is for a two-group comparison, but if we use additional covariates, we need to use a generalized linear model (GLM) framework. FDR‐adjusted P‐values will be used to select differentially expressed genes.

7. Pathway enrichment analysis - Gene Set Enrichment Analysis

Data and scripts availability under oar (Dr. Goodenow) group at Locus Computing Cluster, NIH/NIAID.
1. Raw and processed data present in /hpcdata/oar directory.
2. Scripts present in /nethome/dinasarapuar/nida/script directory.



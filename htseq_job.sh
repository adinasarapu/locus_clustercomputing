#!/bin/sh

echo "Start - `date`"

BAM_INDEXING=True

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /hpcdata/scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /hpcdata/scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR

rsync -av $BAM $TMPDIR/

module load samtools

# create bam index file 
if [ "$BAM_INDEXING" = True ]; then
 samtools index $TMPDIR/Aligned.sortedByCoord.out.bam
fi

module unload samtools

# copy gtf into $TMPDIR
rsync -av /hpcdata/bio_data/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf $TMPDIR/

/usr/bin/mkdir -p $TMPDIR/htseq/$SID

module load htseq

htseq-qa -t bam -o $TMPDIR/htseq/$SID $TMPDIR/Aligned.sortedByCoord.out.bam
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $TMPDIR/Aligned.sortedByCoord.out.bam $TMPDIR/genes.gtf > $TMPDIR/htseq/$SID/${SID}.counts

module unload htseq

# copy results
rsync -av $TMPDIR/htseq/$SID $OUT/

# remove the temp directory
/bin/rm -fr $TMPDIR

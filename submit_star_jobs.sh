#!/bin/sh

# run this script as
# sh submit_star_jobs.sh

DATA_HOME=/hpcdata/oar
SCRIPT=$HOME/nida/script/star_job.sh
FASTQ_DIR=$DATA_HOME/ashok/nida/out/fastq_batch2

# 4threaded (multiples of 4)
threads=12

OUT=$DATA_HOME/ashok/nida/out/mapping
if [ ! -d $OUT ]; then
 mkdir -p $OUT
fi

# get sample IDs from file names
list=$(ls $FASTQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)
# list="10215 110442 40107 40185-1"
# list="HC0033"	

echo $list

for file in $list; do
 echo "Job submitted for sample ${file} ..."
 R1=$FASTQ_DIR/${file}_R1.fastq.gz
 R2=$FASTQ_DIR/${file}_R2.fastq.gz
 echo $R1 " --> " $R2
 qsub -v DATA_HOME=$DATA_HOME,SID=$file,R1=$R1,R2=$R2,OUT=$OUT,t=$threads \
  -N "STAR.${file}.batch2" \
  -pe 12threaded $threads \
  -j y -m abe -M ashok.dinasarapu@nih.gov \
  -cwd $SCRIPT
 sleep 5
done

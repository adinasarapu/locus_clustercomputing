#!/bin/sh

echo "Start - `date`"

BAM_INDEXING=True

#module load OpenMPI
module load STAR/2.7.0f-goolf-1.7.20

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /hpcdata/scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /hpcdata/scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR
/usr/bin/mkdir -p $TMPDIR/{fastq,ref}

rsync -av $R1 $TMPDIR/fastq/
rsync -av $R2 $TMPDIR/fastq/

# copy genome.ga into $TMPDIR
rsync -av /hpcdata/bio_data/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa $TMPDIR/ref/
rsync -av /hpcdata/bio_data/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf $TMPDIR/ref/

# After STAR runs, you should have a few files in the /STAR_mm10/ directory:
# Genome, genomeParameters.txt, chrLength.txt, chrNameLength.txt,
# chrStart.txt, SA, SAindex, Log.out

# STAR indexing of ref genome
# --genomeDir path to the directory where genome files are stored

/usr/bin/mkdir -p $TMPDIR/{STAR_hg38,MAPPING}

STAR --runMode genomeGenerate \
 --genomeDir $TMPDIR/STAR_hg38 \
 --genomeFastaFiles $TMPDIR/ref/genome.fa \
 --outTmpDir $TMPDIR/index.tmp \
 --runThreadN $t

# Alignment to genome (or transcriptome)

/usr/bin/mkdir -p $TMPDIR/MAPPING/${SID}

# STAR will generate:
# Log.out
# Log.progress.out
# Log.final.out summary mapping statistics will give you an idea on qc
# Aligned.sortedByCoord.out.bam â€“ sorted by coordinate â€“ similar to samtools sort command
# SJ.out.tab high confidence splice junction in tab-delimited format
# 
# *** STAR supports multi-line FASTA, so if you convert your multi-line
# FASTQ into FASTA, it should be OK ***.

# --genomeDir path to the directory where genome files are stored
# --readFilesIn paths to files that contain input read
# --outSAMunmapped output of unmapped reads in the SAM format, None or Within SAM file
# --outSAMunmapped output of unmapped reads in the SAM format, None or Within SAM file
# --quantMode types of quantification requested, i.e. GeneCounts or TranscriptomeSAM
# --twopassMode 2-pass mapping mode. 

# STAR outputs read counts per gene into ReadsPerGene.out.tab file with 4 columns which correspond to different strandedness options:
# column 1: gene ID
# column 2: counts for unstranded RNA-seq
# column 3: counts for the 1st read strand aligned with RNA (htseq-count option -s yes)
# column 4: counts for the 2nd read strand aligned with RNA (htseq-count option -s reverse)
# Select the output according to the strandedness of your data. 
# Note, that if you have stranded data and choose one of the columns 3 or 4, the other column (4 or 3) will give you the count of antisense reads. 

# With --quantMode TranscriptomeSAM GeneCounts, and get both the Aligned.toTranscriptome.out.bam and ReadsPerGene.out.tab outputs.

STAR --runMode alignReads --quantMode TranscriptomeSAM GeneCounts \
 --genomeDir $TMPDIR/STAR_hg38/ \
 --readFilesIn $TMPDIR/fastq/${SID}_R1.fastq.gz $TMPDIR/fastq/${SID}_R2.fastq.gz \
 --readFilesCommand zcat \
 --sjdbGTFfile $TMPDIR/ref/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outTmpDir $TMPDIR/map.tmp \
 --outReadsUnmapped Fastx \
 --outFileNamePrefix $TMPDIR/MAPPING/${SID}/ \
 --runThreadN $t

#--outTmpDir $TMPDIR/map.tmp
module load picard/2.17.6
module load samtools/1.9-goolf-1.7.20

# Mark duplicates with Picard
# This creates a sorted BAM file called xxxx.dedupe.bam with the same 
# content as the input file, except that any duplicate reads are marked 
# as such. It also produces a metrics file called xxxx.metrics

# JAVA_ARGS="-Xms512m -Xmx16G -Djava.io.tmpdir=$TMPDIR"

java -jar ${EBROOTPICARD}/picard.jar MarkDuplicates MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
 METRICS_FILE=$TMPDIR/MAPPING/${SID}/${SID}.picard.metrics \
 REMOVE_DUPLICATES=true \
 ASSUME_SORTED=true \
 VALIDATION_STRINGENCY=LENIENT \
 INPUT=$TMPDIR/MAPPING/${SID}/Aligned.sortedByCoord.out.bam \
 OUTPUT=$TMPDIR/MAPPING/${SID}/Aligned.sortedByCoord.dedupe.bam
		
# create bam index file 
if [ "$BAM_INDEXING" = True ]; then
 samtools index $TMPDIR/MAPPING/${SID}/Aligned.sortedByCoord.out.bam
 samtools index $TMPDIR/MAPPING/${SID}/Aligned.sortedByCoord.dedupe.bam
fi

module unload picard/2.17.6
module unload samtools/1.9-goolf-1.7.20

/bin/rm -fr $TMPDIR/ref

# copy results
rsync -av $TMPDIR/MAPPING/${SID} $OUT/

# remove the temp directory
/bin/rm -fr $TMPDIR

# copy log files 
mv $HOME/nida/logs/STAR.${SID}.* $DATA_HOME/ashok/logs/
sleep 10

## module unload OpenMPI
module unload STAR/2.7.0f-goolf-1.7.20

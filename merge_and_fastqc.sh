#!/bin/sh

echo "Start - `date`"

## qconf -spl
## qconf -sp <pe_name>

## allocation rule, defines how to assign slots to a job

## $pe_slots 	will allocate all slots for that job on a single host
## $fill_up	If a user requests 8 slots and a single machine has 8 slots available, that job will run entirely on one machine. 
## 		If 5 slots are available on one host and 3 on another, it will take all 5 on that host, and all 3 on the other host. 
## $round_robin	From all suitable hosts a single slot is allocated until all tasks requested by the parallel job are dispatched. 
## 		If more tasks are requested than suitable hosts are found, allocation starts again from the  first  host

#$ -N batch1_fastqc_round_20

## slots - the maximum number of job slots that the parallel environment is allowed to occupy at once
#$ -pe round 20
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email_id>

module load OpenMPI

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

DATA_HOME=/hpcdata/oar
OUT_DIR=$DATA_HOME/ashok/nida

SEQ_DIR=$DATA_HOME/data

# get sample IDs from file names
list=$(ls $SEQ_DIR/FASTQ_Generation_2017*/*L00*/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)

if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /hpcdata/scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /hpcdata/scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR
/usr/bin/mkdir -p $TMPDIR/{fastq,fastqc,data}

OUT_FASTQ_DIR=$OUT_DIR/out/fastq_batch1
if [ ! -d $OUT_FASTQ_DIR ]; then
 mkdir -p $OUT_FASTQ_DIR
fi

OUT_FASTQC_DIR=$OUT_DIR/out/fastqc_batch1
if [ ! -d $OUT_FASTQC_DIR ]; then
 mkdir -p $OUT_FASTQC_DIR
fi

rsync -av $SEQ_DIR/FASTQ_Generation_2017* $TMPDIR/data/

module load FastQC/0.11.5-Java-1.8.0_45

for file in $list; do
 echo "ID --> ${file}"
 for j in 1 2; do
  
  Rall=$TMPDIR/data/FASTQ_Generation_*/*-${file}-*/${file}_*_R${j}_001.fastq.gz 
  Rcat=$TMPDIR/fastq/${file}_R${j}.fastq.gz

  zcat $Rall | gzip -c > $Rcat
  fastqc -t 20 $Rcat -o $TMPDIR/fastqc

  /bin/rm $Rall

  rsync -av $Rcat $OUT_FASTQ_DIR
  rsync -av $TMPDIR/fastqc/$file* $OUT_FASTQC_DIR

  /bin/rm $Rcat
  /bin/rm $TMPDIR/fastqc/$file*

 done
done

/bin/rm -fr $TMPDIR

module unload FastQC/0.11.5-Java-1.8.0_45
module unload OpenMPI

echo "Finish - `date`"

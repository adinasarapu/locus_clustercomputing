#!/bin/sh

DATA_HOME=/hpcdata/oar
SCRIPT=$HOME/nida/script/htseq_job.sh
MAPPING_DIR=$DATA_HOME/ashok/nida/out/mapping

# 4threaded (multiples of 4)
threads=1

OUT=$DATA_HOME/ashok/nida/out/htseq
if [ ! -d $OUT ]; then
 mkdir -p $OUT
fi

# get sample IDs from file names
list=$(ls $MAPPING_DIR | xargs -n1 basename | sort | uniq)

echo $list

for file in $list; do

 echo "Job submitted for sample ${file} ..."
 BAM=$MAPPING_DIR/${file}/Aligned.sortedByCoord.out.bam
 echo $BAM

 qsub -v SID=$file,BAM=$BAM,OUT=$OUT,t=$threads \
  -N "HTSeq.${file}" \
  -pe threaded $threads \
  -j y -m abe -M <your email address> \
  -cwd $SCRIPT

 sleep 5

done
